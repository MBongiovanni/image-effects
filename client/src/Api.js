/**
 * configurazione axios 
 */
import axios from 'axios'

const SERVER_URL = "http://localhost:9090";

const instance = axios.create({
    baseURL :  SERVER_URL,
    timeout : 30000
})

export default{
    /**
     * metodo di test, ottieni una stringa dal server spring
     */
    getHello: ()=>instance.get('helloword'),

    /**
     * Effettua l'upload dell'immagine selezionata
     */
    pushImage: (data)=>{
        var postdata = new URLSearchParams();
        postdata.append("dataUrl",data);

        return instance.post('loadimage', postdata);
    },
    mosaicEffect: (data)=>{
        var postdata = new URLSearchParams();
        postdata.append('cell_width', data.cellWidth);
        postdata.append('prox_modifier', data.proxModifier);
        postdata.append('show_edges', data.showEdges);
        postdata.append('show_superpixel', data.showSuperpixel);

        return instance.post("mosaic", postdata);
    },
    popEffect: (data)=>{
        var postdata = new URLSearchParams();
        postdata.append("is_inverted", data.isInverted);
        postdata.append("bi_d",data.bilateralFilther.d);
        postdata.append("bi_sigmaColor",data.bilateralFilther.sigmaColor);
        postdata.append("bi_sigmaSpace",data.bilateralFilther.sigmaSpace);
        postdata.append("module",data.module);
        postdata.append("blackThreshold",data.blackThreshold);
        postdata.append("whiteThreshold", data.whiteThreshold);
        postdata.append("bin_blockSize",data.binaryImage.blockSize);
        postdata.append("bin_c",data.binaryImage.c);
        
        return instance.post("pop", 
            postdata,
        );
    }

}