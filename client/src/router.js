import Vue from 'vue'
import Router from 'vue-router'
import Loader from './components/MainComponent.vue'
import PopEffect from './components/PopEffect.vue'
import ComicEffect from './components/ComicEffect.vue'
import MosaicEffect from './components/MosaicEffect.vue'
import OilEffect from './components/OilEffect.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'load_image',
      component: Loader
    },
    {
      path: '/pop',
      name: 'pop_effect',
      component: PopEffect
    },
    {
      path: '/comic',
      name: 'comic_effect', 
      component: ComicEffect
    },
    {
      path: '/mosaic',
      name: 'mosaic_effect',
      component: MosaicEffect
    },
    {
      path: '/oil',
      name: "oil_effect",
      component: OilEffect
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
})
