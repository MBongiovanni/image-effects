import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueLogger from 'vuejs-logger';
import vuetify from './plugins/vuetify';
// import * as dat from 'dat.gui';
// import Phaser from 'phaser/src/phaser';
import ImageUploader from 'vue-image-upload-resize';
import Vuex from 'vuex'
import Toasted from 'vue-toasted';

Vue.use(Vuex)
Vue.use(Toasted,{
  position: "top-center",
  theme: "outline",
  duration : 3000
})


const store = new Vuex.Store({
  state: {
    image: {
      dataUrl: ""
    },
    message: null,
    alertType: null,
    // canvasGui: null,
    imageLoaded: false,
  },
  getters: {
    image: (state)=>{ return state.image; },
    imageEncoded : (state)=>{ return state.image.dataUrl; },
    message: (state)=>{ return state.message; },
    alertType: (state)=>{ return state.alertType; },
    // canvasGui: (state)=>{
    //   if (state.canvasGui != null) {
    //     console.log("Destroying existing gui . . . ");
    //     state.canvasGui.destroy();
    //   }
    //   console.log("Creating a new GUI . . . ");
    //   state.canvasGui = new dat.GUI({autoPlace: false});
    //   return state.canvasGui;
    // },
    imageLoaded: (state)=>{ return state.imageLoaded}
  },
  actions: {

  },
  mutations:{
    image: (state, image)=>{ state.image = image; },
    message: (state, message)=>{ state.message = message; },
    alertType:(state, alertType)=>{ state.alertType = alertType; },
    // canvasGui: (state, canvasGui)=>{ state.canvasGui = canvasGui; },
    imageLoaded: (state, isLoaded)=>{ state.imageLoaded = isLoaded}
  }
})


const options = {
  isEnabled: true,
  logLevel : 'debug',
  stringifyArguments : false,
  showLogLevel : true,
  showMethodName : false,
  separator: '|',
  showConsoleColors: true
};

Vue.use(ImageUploader);
Vue.use(VueLogger, options);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  // Phaser,
  render: h => h(App)
}).$mount('#app')