package it.multimedia.effects;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import nu.pattern.OpenCV;

@SpringBootApplication
public class Effects1Application {
	public static void main(String[] args) {
		SpringApplication.run(Effects1Application.class, args);
		OpenCV.loadShared();
	}

}
