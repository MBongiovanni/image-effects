package it.multimedia.effects.classes;

/**
 * PopOpts
 */
public class PopOpts{
    public class BilateralFiltherOpts{
        public Integer d;
        public Integer sigmaColor;
        public Integer sigmaSpace;

        BilateralFiltherOpts(){}

        BilateralFiltherOpts(Integer d, Integer sigmaColor, Integer sigmaSpace){
            this.d = d;
            this.sigmaColor = sigmaColor;
            this.sigmaSpace = sigmaSpace;
        }

        public Integer getD() {
            return d;
        }

        public void setD(Integer d) {
            this.d = d;
        }

        public Integer getSigmaColor() {
            return sigmaColor;
        }

        public void setSigmaColor(Integer sigmaColor) {
            this.sigmaColor = sigmaColor;
        }

        public Integer getSigmaSpace() {
            return sigmaSpace;
        }

        public void setSigmaSpace(Integer sigmaSpace) {
            this.sigmaSpace = sigmaSpace;
        }
    }

    public class Color{
        Double alpha;
        Double beta;
        Double gamma;
        public Color(){}
        public Color(Double alpha, Double beta, Double gamma){
            this.alpha = alpha;
            this.beta = beta;
            this.gamma = gamma;
        }

        public Double getAlpha() {
            return alpha;
        }

        public void setAlpha(Double alpha) {
            this.alpha = alpha;
        }

        public Double getBeta() {
            return beta;
        }

        public void setBeta(Double beta) {
            this.beta = beta;
        }

        public Double getGamma() {
            return gamma;
        }

        public void setGamma(Double gamma) {
            this.gamma = gamma;
        }
        
    }
    
    public class BinaryImage{
        Integer blockSize;
        Double c;
        public BinaryImage(){}
        public BinaryImage(Integer blockSize, Double c){
            this.blockSize = blockSize;
            this.c = c;
        }

        public Integer getBlockSize() {
            return blockSize;
        }

        public void setBlockSize(Integer blockSize) {
            this.blockSize = blockSize;
        }

        public Double getC() {
            return c;
        }

        public void setC(Double c) {
            this.c = c;
        }
        
    }

    public Boolean isInverted;
    public BilateralFiltherOpts bilateralFiltherOpts;
    public Integer module;
    public Integer blackThreshold;
    public Integer whiteThreshold;
    public BinaryImage binaryImage;

    public PopOpts( Boolean isInverted, Integer d, Integer sigmaColor, Integer sigmaSpace, Integer module, Integer blackThreshold, Integer whiteThreshold,Integer blockSize, Double c ){
        this.isInverted = isInverted ;
        this.bilateralFiltherOpts = new PopOpts.BilateralFiltherOpts(d, sigmaColor, sigmaSpace);
        this.module = module;
        this.blackThreshold = blackThreshold;
        this.binaryImage = new BinaryImage(blockSize, c);
        this.whiteThreshold = whiteThreshold;
    }
    public PopOpts(){}

    public BilateralFiltherOpts getBilateralFiltherOpts() {
        return bilateralFiltherOpts;
    }

    public void setBilateralFiltherOpts(BilateralFiltherOpts bilateralFiltherOpts) {
        this.bilateralFiltherOpts = bilateralFiltherOpts;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getBlackThreshold() {
        return blackThreshold;
    }

    public void setBlackThreshold(Integer blackThreshold) {
        this.blackThreshold = blackThreshold;
    }

    public BinaryImage getBinaryImage() {
        return binaryImage;
    }

    public void setBinaryImage(BinaryImage binaryImage) {
        this.binaryImage = binaryImage;
    }

    public Boolean getIsInverted() {
        return isInverted;
    }

    public void setIsInverted(Boolean isInverted) {
        this.isInverted = isInverted;
    }   
    
}