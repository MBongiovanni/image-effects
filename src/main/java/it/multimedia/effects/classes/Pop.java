package it.multimedia.effects.classes;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
/**
 * Pop Effect
 */
public class Pop {

    public Pop(){}
    
    public byte[] popEffect(Mat originalImage, CustomImage cImage, PopOpts opts) {
		System.out.println("Pop Effect called : ");

		Mat image = new Mat();
		originalImage.copyTo(image);

		///////////////////////////////////////// applying bilateral filther twice to the original image
		Mat outBilateral = new Mat();
		Mat outBilateral_ = new Mat();
		// Imgproc.bilateralFilter( //originale
		// 	image,  
		// 	outBilateral,
		// 	opts.bilateralFiltherOpts.d, 
		// 	opts.bilateralFiltherOpts.sigmaColor, 
		// 	opts.bilateralFiltherOpts.sigmaSpace
		// );
		
		Imgproc.bilateralFilter( // alternativa
			image,  
			outBilateral_,
			opts.bilateralFiltherOpts.d, 
			opts.bilateralFiltherOpts.sigmaColor, 
			opts.bilateralFiltherOpts.sigmaSpace
		);
		Imgproc.bilateralFilter( 
			outBilateral_,  
			outBilateral, 
			opts.bilateralFiltherOpts.d, 
			opts.bilateralFiltherOpts.sigmaColor, 
			opts.bilateralFiltherOpts.sigmaSpace
		);
		int rowCount = outBilateral.rows();
		int colCount = outBilateral.cols();

		outBilateral = normalize(outBilateral, 255, rowCount, colCount);
		////////////////////////////////////////				
		 
		
		
		
		// int module = opts.module;
		
		double [][][] rgbImageDouble = new double[rowCount][colCount][4];
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				rgbImageDouble[i][j] = outBilateral.get(i,j);
			}
		}

		///////////////////////////////////////// looking for detail
		Integer blackThreshold = opts.blackThreshold;
		Mat quantizedDetail =  new Mat();

		outBilateral.copyTo(quantizedDetail);
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				double[] color = quantizedDetail.get(i, j);
				//if none of the color channel is greater than the threshold i put to 0 the color.
				if (!(color[0] > blackThreshold && color[1] > blackThreshold && color[2] > blackThreshold)) {

					color[0] = 0;
					color[1] = 0;
					color[2] = 0;
				}
				quantizedDetail.put(i,j, color);
			}
		}

		// 
		Mat lutDetail = new Mat(1, 256, CvType.CV_8UC1);	  
		
		lutDetail.setTo(new Scalar(0));
		for (int i = 0; i < 256; i++) {
			lutDetail.put(0, i, (i-(i%opts.module)));
		}
		Core.LUT(quantizedDetail, lutDetail, quantizedDetail);
		//

		quantizedDetail = normalize(quantizedDetail, 255, rowCount, colCount);
		/////////////////////////////////////////


		///////////////////////////////////////// looking for background
		Mat quantizedBackground =  new Mat();

		outBilateral.copyTo(quantizedBackground);

		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				double[] color = quantizedBackground.get(i, j);

				color[2] = rgbImageDouble[i][j][2] ;
				color[1] = rgbImageDouble[i][j][1] ;
				color[0] = rgbImageDouble[i][j][0] ;

				if( ((color[0] > color[1]) && (color[0] > color[2]))){
					if (color[0] > opts.blackThreshold) {
						color[0] = 200;
						color[1] = 0;
						color[2] = 0;
					}else{
						color[0] = 0;
						color[1] = 0;
						color[2] = 0;
					}
				}else if( ((color[1] > color[0]) && (color[1] > color[2]))){
					// color[1] = 200;
					// color[0] = 0;
					// color[2] = 0;
					if (color[1] > opts.blackThreshold) {
						color[1] = 200;
						color[0] = 0;
						color[2] = 0;
					}else{
						color[0] = 0;
						color[1] = 0;
						color[2] = 0;
					}
				}else if( ((color[2] > color[1]) && (color[2] > color[0]))){
					// color[2] = 200;
					// color[1] = 0;
					// color[0] = 0;
					if (color[2] > opts.blackThreshold) {
						color[2] = 200;
						color[1] = 0;
						color[0] = 0;
					}else{
						color[0] = 0;
						color[1] = 0;
						color[2] = 0;
					}
				}else{
					color[0] = 0;
					color[1] = 0;
					color[2] = 0;
				}
				quantizedBackground.put(i,j,color);
			}
		}

		quantizedBackground = normalize(quantizedBackground, 255, rowCount, colCount);

		/////////////////////////////////////////
		
		///////////////////////////////////////// sum background and details
		Mat mergedColor = new Mat();
		originalImage.copyTo(mergedColor);
		// Core.addWeighted(quantizedDetail, opts.color.alpha, quantizedBackground, opts.color.beta, opts.color.gamma, mergedColor); //originale //sostituire addWeighted con altro

		/**
		 * idea di base : 
		 * 	sommare i due colori, se la loro somma è  > di una certa "white threshold" e il dettaglio non è bianco, mantenere solo il dettaglio;
		 * 	in caso contrario prendere solo il background
		 */
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				double[] detailColor = quantizedDetail.get(i, j);
				double[] backgroundColor = quantizedBackground.get(i, j);

				double[] tempColor = quantizedBackground.get(i, j);

				if((detailColor[0]+backgroundColor[0] > opts.whiteThreshold) && (detailColor[1]+backgroundColor[1] > opts.whiteThreshold) && (detailColor[2]+backgroundColor[2] > opts.whiteThreshold)){
					if (detailColor[0] > opts.whiteThreshold && detailColor[1] > opts.whiteThreshold && detailColor[2] > opts.whiteThreshold) {
						tempColor = quantizedBackground.get(i, j);
					}else{
						tempColor = quantizedDetail.get(i, j);
					}
				}else{
					tempColor[0] = detailColor[0]+backgroundColor[0];
					tempColor[1] = detailColor[1]+backgroundColor[1];
					tempColor[2] = detailColor[2]+backgroundColor[2];
				}

				mergedColor.put(i, j, tempColor);
			}
		}

		mergedColor = normalize(mergedColor, 255, rowCount, colCount);
		/////////////////////////////////////////

		///////////////////////////////////////// calculating borders
		Mat greyImage = new Mat();
		Mat greyImageOut = new Mat();
		Imgproc.cvtColor(outBilateral, greyImage, Imgproc.COLOR_BGR2GRAY); //original
		Imgproc.adaptiveThreshold(greyImage, greyImage, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, opts.binaryImage.blockSize, opts.binaryImage.c); // ok


		Imgproc.cvtColor(greyImage, greyImageOut, Imgproc.COLOR_GRAY2BGR);
		/////////////////////////////////////////

		///////////////////////////////////////// getting the output
		Mat output = new Mat();

		if (opts.isInverted) {
			Core.add(greyImageOut, mergedColor, output); 
		}else{
			/**
			 * idea di base : 
			 * 	- Se il pixel dell'immagine b/n è bianca, si prende lo sfondo, altrimenti si mette il nero.
			 */
			originalImage.copyTo(output);
			for (int i = 0; i < rowCount; i++) {
				for (int j = 0; j < colCount; j++) {
					double[] color = mergedColor.get(i, j);
					double[] greyImgColor = greyImageOut.get(i, j);
					double[] tempColor = mergedColor.get(i, j);
					
					if ((greyImgColor[0] > opts.whiteThreshold) && (greyImgColor[1] > opts.whiteThreshold) && (greyImgColor[2] > opts.whiteThreshold)) {
						tempColor = color;
					} else {
						tempColor = greyImgColor;
					}
	
					output.put(i, j, tempColor);
				}
			}
		}

		output = normalize(output, 255, rowCount, colCount);

		// return cImage.matToByte(originalImage, cImage.imageExt);
		// return cImage.matToByte(greyImageOut, cImage.imageExt);
		// return cImage.matToByte(outBilateral, cImage.imageExt);
		// return cImage.matToByte(image, cImage.imageExt);
		// return cImage.matToByte(quantizedDetail, cImage.imageExt);
		// return cImage.matToByte(quantizedBackground, cImage.imageExt);
		// return cImage.matToByte(mergedColor, cImage.imageExt);
		return cImage.matToByte(output, cImage.imageExt); //original
		/////////////////////////////////////////

	}
	
	public Mat normalize(Mat src, Integer range, Integer rowCount, Integer colCount){
		Mat dst = new Mat();
		src.copyTo(dst);
		Double maxR = 0.0;
		Double minR = 0.0;
		Double maxG = 0.0;
		Double minG = 0.0;
		Double maxB = 0.0;
		Double minB = 0.0;
		//looking for max and min per channel
		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				double[] color = src.get(i, j);
				if (maxR < color[0]) {
					maxR = color[0];
				}

				if (maxG < color[1]) {
					maxG = color[1];
				}

				if (maxB < color[2]) {
					maxB = color[2];
				}

				if (minR > color[0]) {
					minR = color[0];
				}

				if (minG > color[1]) {
					minG = color[1];
				}

				if (minB > color[2]) {
					minB = color[2];
				}
			}
		}


		for (int i = 0; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				double[] color = src.get(i,j);

				color[0] = ((color[0] - minR)/(maxR - minR)) * range;
				color[1] = ((color[1] - minG)/(maxG - minG)) * range;
				color[2] = ((color[2] - minB)/(maxB - minB)) * range;

				dst.put(i,j, color);
			}
		}
		return dst;
	}
}