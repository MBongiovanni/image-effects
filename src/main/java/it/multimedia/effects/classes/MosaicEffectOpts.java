package it.multimedia.effects.classes;

/**
 * MosaicEffectOpts
 */
public class MosaicEffectOpts {
    /*
    * @param S cellwidith (1-255)
    * @param m proximity modifier (1-255)
    * @param showEdges show edge of each cluster
    * @param showSuperpixels show a red dot inside the center of each cluster
    */

    public Integer cellWidth;
    public Integer proximityModifier;
    public Boolean showEdges;
    public Boolean showSuperpixel;

    public MosaicEffectOpts(){}

    public MosaicEffectOpts(Integer cellWidth, Integer proximityModifier, Boolean showEdges, Boolean showSuperpixel){
        this.cellWidth = cellWidth;
        this.proximityModifier = proximityModifier;
        this.showEdges = showEdges;
        this.showSuperpixel = showSuperpixel;
    }

    public Integer getCellWidth() {
        return cellWidth;
    }

    public void setCellWidth(Integer cellWidth) {
        this.cellWidth = cellWidth;
    }

    public Integer getProximityModifier() {
        return proximityModifier;
    }

    public void setProximityModifier(Integer proximityModifier) {
        this.proximityModifier = proximityModifier;
    }

    public Boolean getShowEdges() {
        return showEdges;
    }

    public void setShowEdges(Boolean showEdges) {
        this.showEdges = showEdges;
    }

    public Boolean getShowSuperpixel() {
        return showSuperpixel;
    }

    public void setShowSuperpixel(Boolean showSuperpixel) {
        this.showSuperpixel = showSuperpixel;
    }
    
}