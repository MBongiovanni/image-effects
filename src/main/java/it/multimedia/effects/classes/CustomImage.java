package it.multimedia.effects.classes;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.Base64;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

public class CustomImage {
	
	
	public Integer widith;
	public Integer height;
	public Image image; 
	public String byteImage;
	public String outputByteImage;
	public static String inputImagePath = "./savedImages/input.";
	public static String outputImagePath = "./savedImages/output.";
	public String finalInputPath;
	public String finalOutputPath;
	public static String imagesFolderPath = "./savedImages";
	public String imageExt;

	public CustomImage(){}

	public CustomImage(Boolean fromLoadedImage){
		if (fromLoadedImage) {
			System.out.println("loading from remote file . . .");
			Path path = Paths.get(CustomImage.imagesFolderPath);
			File imageFile;
			try (final DirectoryStream<Path> stream = Files.newDirectoryStream(path, "input{,.*}")) {
				imageFile = stream.iterator().next().toFile();

				int i = imageFile.getPath().lastIndexOf('.');
				if (i > 0) {
					this.imageExt = imageFile.getPath().substring(i+1);
				}else{
					throw new Exception("Formato file non riconosciuto.");
				}
				this.finalInputPath = imageFile.getPath();
				this.finalOutputPath = imageFile.getPath().replace("input", "output");
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * encode a image in byte64 format
	 */
	public CustomImage createByteImage(String imageRgx, Boolean isOutput){
		String base64File = "";
		Path path = Paths.get(CustomImage.imagesFolderPath);
		File file;
		try (final DirectoryStream<Path> stream = Files.newDirectoryStream(path, imageRgx)){
			file = stream.iterator().next().toFile();

			try (FileInputStream fis = new FileInputStream(file)) {
				byte fileData[] = new byte[(int) file.length()];
				fis.read(fileData);
				base64File = Base64.getEncoder().encodeToString(fileData);
				if (isOutput) {
					this.outputByteImage = base64File;
				}else{
					this.byteImage = base64File;
				}
				return this;
			} 
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	} 


	/**
	 * Salva l'immagine codificata ricevuta 
	 * @param dataUrl stringa contenente l'immagine codificata in base64
	 */
	public Boolean loadImage(String dataUrl){
		System.out.println("Calling loadImage . . . ");
		//remove previous input images
		Path homePath = Paths.get("./savedImages");
		try (final DirectoryStream<Path> stream = Files.newDirectoryStream(homePath, "*")) {
			stream.forEach((_path)->{
				File toDelete = _path.toFile();
				toDelete.delete();
			});
		}catch(Exception e){
			e.printStackTrace();
		}


		String[] strings = dataUrl.split(",");
        String extension;
        switch (strings[0]) {//check image's extension
            case "data:image/jpeg;base64":
                extension = "jpeg";
                break;
            case "data:image/png;base64":
                extension = "png";
                break;
            default: 
                extension = "jpg";
                break;
		}
		this.imageExt = extension;
        //convert base64 string to binary data
		byte[] data = Base64.getDecoder().decode(strings[1]);
		String path = CustomImage.inputImagePath + this.imageExt;
		File file = new File(path);
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
			outputStream.write(data);
			System.out.println("Done saving. ");
			System.out.println(strings[1].length());
			System.out.println(file.getAbsolutePath());
			return true;
        } catch (IOException e) {
			e.printStackTrace();
			return false;
        }
	}
	
	/**
	 * generate a image with a mosaic effect
	 * @return the instance of this custom image.
	 */
	public CustomImage mosaicEffect(MosaicEffectOpts opts){
		Superpixel spx = new Superpixel();
		BufferedImage img = spx.loadImage(this.finalInputPath);
		BufferedImage output = spx.calculateMosaicImage(img,this.finalOutputPath, this.imageExt, opts.cellWidth, opts.proximityModifier, opts.showEdges, opts.showSuperpixel);
		this.createByteImage("output{,.*}", true);
		String dataExt = "";
		switch (this.imageExt) {
			case "jpeg":
				dataExt = "data:image/jpeg;base64,";
				break;
			case "png":
				dataExt = "data:image/png;base64,";
				break;
	
			default:
				dataExt = "data:image/jpg;base64,";
				break;
		}
		
		//String dataStr = Base64.getEncoder().encodeToString(data);
		this.outputByteImage = dataExt + this.outputByteImage;
		return this;
	}


	/**
	 * Converts encoded byte image to Mat (opencv) 
	 * @param encodedImage string data
	 * @return Mat representation of the byte[] image given
	 */
	public Mat base64ToMat(String encodedImage){
		//convert base64 string to binary data
		System.out.println("Starting conversion from base64 to Mat(opencv)");
		byte[] data = Base64.getDecoder().decode(encodedImage);
		Mat mat = Imgcodecs.imdecode(new MatOfByte(data), Imgcodecs.CV_LOAD_IMAGE_UNCHANGED); /*CV_LOAD_IMAGE_UNCHANGED*/
		return mat; 
	}
	
	/**
	 * Converts Mat image do encoded byte array
	 * @param matImage image in Mat format
	 * @return byte[] representation of the Mat image given
	 */
	public byte[] matToByte(Mat matImage, String imageFormat) {
		System.out.println("matToByte called : ");
		System.out.println(matImage);
		System.out.println(imageFormat);
		MatOfByte mob = new MatOfByte();
	    Imgcodecs.imencode("."+imageFormat, matImage, mob);
	    byte byteImage[] = mob.toArray();
		return byteImage;
	}
	
	public CustomImage popEffect(PopOpts opts) {
		
		Mat inImage = this.base64ToMat(this.byteImage);
		Pop pop = new Pop();
		
		byte [] data = pop.popEffect(inImage, this, opts);
		
		String dataExt = "";
		
		switch (this.imageExt) {
			case "jpeg":
				dataExt = "data:image/jpeg;base64,";
				break;
			case "png":
				dataExt = "data:image/png;base64,";
				break;
	
			default:
				dataExt = "data:image/jpg;base64,";
				break;
		}
		
		//String dataStr = Base64.getEncoder().encodeToString(data);
		this.outputByteImage = dataExt + Base64.getEncoder().encodeToString(data);
		
		//this.loadImage(dataExt+dataStr);
		
		//byte[] data = Base64.getDecoder().decode();
		String path = CustomImage.outputImagePath + this.imageExt;
		File file = new File(path);
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
			outputStream.write(data);
			System.out.println("Done saving output Pop Effect : ");
			System.out.println(file.getAbsolutePath());
        } catch (IOException e) {
			e.printStackTrace();
        }
		
		return this;
	}
}