package it.multimedia.effects;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import it.multimedia.effects.classes.PopOpts;
import it.multimedia.effects.classes.CustomImage;
import it.multimedia.effects.classes.MosaicEffectOpts;


@RestController
@RequestMapping("/")
public class IndexController {

	@Autowired
	ServletContext context;

	@CrossOrigin
	@PostMapping(value = "/loadimage")
	public @ResponseBody Boolean loadImage(@RequestParam(name="dataUrl", required=true)String dataUrl, HttpServletResponse response ){


		try (final DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get("./savedImages") , "*")) {
			stream.forEach((_path)->{
				System.out.println(_path.toString());
			});
		}catch(Exception e){
			e.printStackTrace();
		}
		return new CustomImage().loadImage(dataUrl);
	}


	@CrossOrigin
	@PostMapping("/mosaic")
	public CustomImage mosaicEffect(
		@RequestParam(name="cell_width") Integer cellWidth,
		@RequestParam(name = "prox_modifier") Integer proxModifier,
		@RequestParam(name = "show_edges") Boolean showEdges,
		@RequestParam(name = "show_superpixel") Boolean showSuperpixel
	){
		MosaicEffectOpts opts = new MosaicEffectOpts(cellWidth, proxModifier, showEdges, showSuperpixel);
		System.out.println("Mosaic Effect Called");
		CustomImage cm = new CustomImage(true).mosaicEffect(opts);
		return cm;
	}

	@CrossOrigin
	@PostMapping(path="/pop")
	public CustomImage popEffect(
		@RequestParam(name ="is_inverted") Boolean isInverted,
		@RequestParam(name="bi_d") Integer d, 
		@RequestParam(name="bi_sigmaColor") Integer sigmaColor, 
		@RequestParam(name="bi_sigmaSpace") Integer sigmaSpace,
		@RequestParam(name="module") Integer module,
		@RequestParam(name="blackThreshold") Integer blackThreshold,
		@RequestParam(name = "whiteThreshold") Integer whiteThreshold,
		@RequestParam(name="bin_blockSize") Integer blockSize,
		@RequestParam(name="bin_c") Double c
	){
		System.out.println(d);
		System.out.println(isInverted);
		System.out.println(sigmaSpace);
		System.out.println(module);

		System.out.println("Pop Effect Called");
		PopOpts opts = new PopOpts(isInverted, d, sigmaColor, sigmaSpace, module, blackThreshold, whiteThreshold,blockSize, c);

		CustomImage cm = new CustomImage(true).createByteImage("input{,.*}", false).popEffect(opts);
		return cm;
	}
}
